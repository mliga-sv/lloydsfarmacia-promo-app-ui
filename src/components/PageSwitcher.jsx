import { Tabs, Layout } from "@shopify/polaris";
import { useState, useCallback, useMemo } from "react";
import CalendarPage from "../pages/CalendarPage";
import ChangeRegistryPage from "../pages/ChangeRegistryPage";
import PricesLogPage from "../pages/PricesLogPage";

import PromoListingPage from "../pages/PromoListingPage";

export default function PageSwitcher() {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    []
  );

  const tabs = [
    {
      id: "calendario-promozioni",
      content: "Calendario promozioni",
      panelID: "calendario-promozioni-content",
    },
    {
      id: "elenco-promozioni",
      content: "Elenco promozioni",
      panelID: "elenco-promozioni-content",
    },
    {
      id: "modifiche-prezzi",
      content: "Modifiche prezzi",
      panelID: "modifiche-prezzi-content",
    },
    {
      id: "registro-modifiche",
      content: "Registro modifiche",
      panelID: "registro-modifiche-content",
    },
    {
      id: "debug",
      content: "Debug",
      panelID: "debug-content",
    },
  ];

  const renderTabContent = useMemo(() => {
    switch (selected) {
      case 4:
      case 3:
        return <ChangeRegistryPage />;
      case 2:
        return <PricesLogPage />;
      case 1:
        return <PromoListingPage />;
      case 0:
        return <CalendarPage />;
      default:
        return <p>case {selected} not found</p>;
    }
  }, [selected]);

  return (
    <Layout fullWidth>
      <Layout.Section>
        <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
          {renderTabContent}
        </Tabs>
      </Layout.Section>
    </Layout>
  );
}
