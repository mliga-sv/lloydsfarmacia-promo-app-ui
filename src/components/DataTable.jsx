import { useState, useMemo, useCallback, useEffect } from "react";
import {
  DataTable as PolarisDataTable,
  Filters,
  Card,
  ChoiceList,
} from "@shopify/polaris";
import { parse, compareDesc, compareAsc } from "date-fns";

export default function DataTable({ columns, rows, filters = [] }) {
  const [filteredRows, setFilteredRows] = useState(rows);
  const [queryValue, setQueryValue] = useState(null);
  const [filtersRegistry, setFiltersRegistry] = useState({});

  const isSearchEnabled = !filters.find((f) => f.key === "search");

  const clearFilter = (key) => {
    if (key) {
      const cloneRegistry = { ...filtersRegistry };
      delete cloneRegistry[key];
      setFiltersRegistry(cloneRegistry);
    }
  };

  const transformedFilters = filters
    .filter((f) => f.key !== "search")
    .map((f) => {
      let filterComponent;

      if (f.filterConfig) {
        switch (f.filterConfig.type) {
          case "choice":
          default:
            filterComponent = (
              <ChoiceList
                title={f.label}
                choices={f.filterConfig.items}
                onChange={(val) => {
                  if (val.length === 0) {
                    clearFilter(f.key);
                  } else {
                    setFiltersRegistry({ ...filtersRegistry, [f.key]: val });
                  }
                }}
                allowMultiple={f.filterConfig.allowMultiple}
                selected={filtersRegistry[f.key] || []}
              />
            );
        }
      }

      return {
        key: f.key,
        label: f.label,
        filter: filterComponent,
        shortcut: f.shortcut,
      };
    });

  const appliedFilters = useMemo(() => {
    return Object.keys(filtersRegistry).map((k) => ({
      key: k,
      label: `${k}: ${filtersRegistry[k].join(", ")}`,
      onRemove: () => clearFilter(k),
    }));
  }, [
    Object.keys(filtersRegistry).map(
      (filter) => filtersRegistry[filter].length
    ),
  ]);

  useEffect(() => {
    let cloneRows = rows;
    Object.keys(filtersRegistry).map((k) => {
      const mappedColumn = filters.find((f) => f.key === k)?.filterConfig
        ?.mappedColumn;
      cloneRows = cloneRows.filter(
        (row) =>
          filtersRegistry[k].length &&
          filtersRegistry[k].some(
            (filterKey) => row[mappedColumn] === filterKey
          )
      );
    });
    setFilteredRows(cloneRows);
  }, [
    Object.keys(filtersRegistry).map(
      (filter) => filtersRegistry[filter].length
    ),
  ]);

  const handleSort = useCallback(
    (index, direction) => {
      const mappedField = columns[index].mappedField;
      const sortedRows = filteredRows.sort((a, b) => {
        switch (mappedField) {
          case "priorita":
            return direction === "descending"
              ? b[index] - a[index]
              : a[index] - b[index];
          case "datainizio":
          case "datafine":
            return sortDates(b[index], a[index], direction);
          default:
            return direction === "descending"
              ? b[index].localeCompare(a[index])
              : a[index].localeCompare(b[index]);
        }
      });
      setFilteredRows(sortedRows);
    },
    [filteredRows]
  );

  const sortDates = (a, b, direction) => {
    const dateA = parse(a, "dd/MM/yyyy", new Date());
    const dateB = parse(b, "dd/MM/yyyy", new Date());
    return direction === "descending"
      ? compareDesc(dateA, dateB)
      : compareAsc(dateA, dateB);
  };

  useEffect(() => {
    filteredRows.map((r) => console.log(r));
  }, [filteredRows]);

  const filterProps = {
    filters: transformedFilters,
    appliedFilters: appliedFilters,
    hideQueryField: isSearchEnabled,
  };

  const columnContentTypes = columns.reduce((acc, obj) => {
    acc.push(obj.type);
    return acc;
  }, []);

  const headings = columns.reduce((acc, obj) => {
    acc.push(obj.heading);
    return acc;
  }, []);

  const sortableOptions = columns.reduce((acc, obj) => {
    acc.push(obj.sortable);
    return acc;
  }, []);

  return (
    <>
      {filters.length > 0 && (
        <Card.Section>
          <Filters {...filterProps} />
        </Card.Section>
      )}

      <PolarisDataTable
        columnContentTypes={columnContentTypes}
        headings={headings}
        rows={filteredRows}
        sortable={sortableOptions}
        defaultSortDirection="descending"
        initialSortColumnIndex={4}
        footerContent={`Showing ${filteredRows.length} of ${rows.length} results`}
        onSort={handleSort}
      />
    </>
  );
}
