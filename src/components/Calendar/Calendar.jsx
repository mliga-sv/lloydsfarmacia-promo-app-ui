import React, { useEffect, useState } from "react";
import "@fullcalendar/react/dist/vdom";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import "./Calendar.css";

export default function Calendar() {
  const [events, setEvents] = useState([
    { title: "Long Event", start: "2022-04-07", end: "2022-04-12" },
    {
      title: "Conference",
      start: "2022-04-13",
      end: "2022-04-15",
      color: "#2d2d2d",
    },
  ]);

  return (
    <FullCalendar
      plugins={[dayGridPlugin]}
      initialView={"dayGridMonth"}
      headerToolbar={{
        left: "",
        center: "prev,title,next",
        right: "dayGridWeek,dayGridMonth",
      }}
      events={events}
      eventClick={() => alert("Event clicked")}
      locale="it"
      weekNumberCalculation="ISO"
    />
  );
}
