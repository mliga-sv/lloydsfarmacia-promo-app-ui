import { useState, useCallback, useMemo } from "react";
import { Page, Card } from "@shopify/polaris";
import DataTable from "../components/DataTable";

export default function PricesLogPage() {
  const data = [
    {
      id: 1,
      prodotto:
        "0.2% Chlorhexidine Mouthwash Colluttorio alla Clorexidina 250 ml",
      minsan: "909089031",
      prezzoPrecedente: "11,90€",
      nuovoPrezzo: "9,50€",
      descrizione: "Lorem ipsum dolor sit amet, consectetur",
      data: "01/04/2022 09:00:00",
    },
    {
      id: 2,
      prodotto:
        "0.2% Chlorhexidine Mouthwash Colluttorio alla Clorexidina 250 ml",
      minsan: "909089031",
      prezzoPrecedente: "11,90€",
      nuovoPrezzo: "9,50€",
      descrizione: "Lorem ipsum dolor sit amet, consectetur",
      data: "01/04/2022 09:00:00",
    },
    {
      id: 3,
      prodotto:
        "0.2% Chlorhexidine Mouthwash Colluttorio alla Clorexidina 250 ml",
      minsan: "909089031",
      prezzoPrecedente: "11,90€",
      nuovoPrezzo: "9,50€",
      descrizione: "Lorem ipsum dolor sit amet, consectetur",
      data: "01/04/2022 09:00:00",
    },
    {
      id: 4,
      prodotto:
        "0.2% Chlorhexidine Mouthwash Colluttorio alla Clorexidina 250 ml",
      minsan: "909089031",
      prezzoPrecedente: "11,90€",
      nuovoPrezzo: "9,50€",
      descrizione: "Lorem ipsum dolor sit amet, consectetur",
      data: "01/04/2022 09:00:00",
    },
  ];

  const dataTableConfig = {
    columns: [
      {
        heading: "Prodotto",
        type: "text",
        sortable: true,
        mappedField: "prodotto",
      },
      {
        heading: "Minsan",
        type: "text",
        sortable: false,
        mappedField: "minsan",
      },
      {
        heading: "Prezzo precedente",
        type: "text",
        sortable: true,
        mappedField: "prezzoPrecedente",
      },
      {
        heading: "Nuovo prezzo",
        type: "text",
        sortable: true,
        mappedField: "nuovoPrezzo",
      },
      {
        heading: "Descrizione variazione",
        type: "text",
        sortable: false,
        mappedField: "descrizione",
      },
      {
        heading: "Data e ora",
        type: "text",
        sortable: true,
        mappedField: "data",
      },
    ],
    filters: [{ key: "search" }],
  };

  const rows = data.map((d) => [
    d.prodotto,
    d.minsan,
    d.prezzoPrecedente,
    d.nuovoPrezzo,
    d.descrizione,
    d.data,
  ]);

  return (
    <Page fullWidth>
      <Card>
        <DataTable
          columns={dataTableConfig.columns}
          filters={dataTableConfig.filters}
          rows={rows}
        />
      </Card>
    </Page>
  );
}
