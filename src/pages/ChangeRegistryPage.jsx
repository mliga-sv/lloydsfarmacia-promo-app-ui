import { useState, useCallback, useMemo } from "react";
import { Page, Card } from "@shopify/polaris";
import DataTable from "../components/DataTable";

export default function ChangeRegistryPage() {
  const data = [
    {
      id: 1,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      dettaglio: "Aggiunta/Rimozione paniere prodotti",
      utente: "Rob M",
      data: "21/04/2022",
    },
    {
      id: 1,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      dettaglio: "Aggiunta/Rimozione paniere prodotti",
      utente: "Gabriele Ciolino",
      data: "01/07/2022",
    },
    {
      id: 1,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      dettaglio: "Aggiunta/Rimozione paniere prodotti",
      utente: "Rob M",
      data: "14/12/2021",
    },
    {
      id: 1,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      dettaglio: "Aggiunta/Rimozione paniere prodotti",
      utente: "Gabriele Ciolino",
      data: "18/02/2022",
    },
  ];

  const dataTableConfig = {
    columns: [
      {
        heading: "Nome promozione",
        type: "text",
        sortable: true,
        mappedField: "nome",
      },
      {
        heading: "Dettaglio modifica",
        type: "text",
        sortable: false,
        mappedField: "dettaglio",
      },
      {
        heading: "Utente",
        type: "text",
        sortable: true,
        mappedField: "utente",
      },
      {
        heading: "Data e ora",
        type: "text",
        sortable: true,
        mappedField: "data",
      },
    ],
    filters: [{ key: "search" }],
  };

  const rows = data.map((d) => [d.nome, d.dettaglio, d.utente, d.data]);

  return (
    <Page fullWidth>
      <Card>
        <DataTable
          columns={dataTableConfig.columns}
          rows={rows}
          filters={dataTableConfig.filters}
        />
      </Card>
    </Page>
  );
}
