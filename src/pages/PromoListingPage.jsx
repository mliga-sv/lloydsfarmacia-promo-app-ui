import { useState, useCallback, useMemo } from "react";
import { Page, Card, Tabs } from "@shopify/polaris";
import DataTable from "../components/DataTable";

export default function PromoListingPage() {
  const [selected, setSelected] = useState(0);

  const data = [
    {
      id: 1,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      meccanica: "XX% sul prodotto meno caro da un paniere",
      priorita: 5,
      stato: "Attiva",
      datainizio: "01/02/2022",
      datafine: "31/03/2022",
      tipologia: "carrello",
    },
    {
      id: 2,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      meccanica: "XX% sul prodotto meno caro da un paniere",
      priorita: 2,
      stato: "In lavorazione",
      datainizio: "05/03/2022",
      datafine: "15/03/2022",
      tipologia: "carrello",
    },
    {
      id: 3,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      meccanica: "XX% sul prodotto meno caro da un paniere",
      priorita: 1,
      stato: "Disattivata",
      datainizio: "24/05/2022",
      datafine: "27/05/2022",
      tipologia: "catalogo",
    },
    {
      id: 4,
      nome: "20220412_20220418_week_prodotti-anticaduta-per-capelli_ecom-1578 30%",
      meccanica: "XX% sul prodotto meno caro da un paniere",
      priorita: 1,
      stato: "Disattivata",
      datainizio: "01/12/2022",
      datafine: "31/01/2023",
      tipologia: "catalogo",
    },
  ];

  // ---------------------

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    []
  );

  const tabs = [
    {
      id: "tutte",
      content: "Tutte",
      panelID: "tutte-content",
    },
    {
      id: "promozioni-carrello",
      content: "Promozioni a carrello",
      panelID: "promozioni-carrello-content",
    },
    {
      id: "promozioni-catalogo",
      content: "Promozioni a catalogo",
      panelID: "promozioni-catalogo-content",
    },
  ];

  const rows = useMemo(() => {
    switch (selected) {
      case 2:
        return data
          .filter((d) => d.tipologia === "catalogo")
          .map((d) => [
            d.nome,
            d.meccanica,
            d.priorita,
            d.stato,
            d.datainizio,
            d.datafine,
          ]);
      case 1:
        return data
          .filter((d) => d.tipologia === "carrello")
          .map((d) => [
            d.nome,
            d.meccanica,
            d.priorita,
            d.stato,
            d.datainizio,
            d.datafine,
          ]);
      case 0:
      default:
        return data.map((d) => [
          d.nome,
          d.meccanica,
          d.priorita,
          d.stato,
          d.datainizio,
          d.datafine,
        ]);
    }
  }, [selected]);

  const dataTableConfig = {
    columns: [
      {
        heading: "Nome",
        type: "text",
        sortable: true,
        mappedField: "nome",
      },
      {
        heading: "Meccanica promo",
        type: "text",
        sortable: false,
        mappedField: "meccanica",
      },
      {
        heading: "Priorità",
        type: "numeric",
        sortable: true,
        mappedField: "priorita",
      },
      {
        heading: "Stato",
        type: "text",
        sortable: false,
        mappedField: "stato",
      },
      {
        heading: "Data inizio",
        type: "text",
        sortable: true,
        mappedField: "datainizio",
      },
      {
        heading: "Data fine",
        type: "text",
        sortable: true,
        mappedField: "datafine",
      },
    ],
    filters: [
      { key: "search" },
      {
        key: "meccanica",
        label: "Meccanica",
        filterConfig: {
          type: "choice",
          mappedColumn: 1,
          items: [
            {
              label: "XX% sul prodotto meno caro da un paniere",
              value: "XX% sul prodotto meno caro da un paniere",
            },
            {
              label: "XX% sul prodotto meno caro da un paniere 111",
              value: "XX% sul prodotto meno caro da un paniere 111",
            },
            {
              label: "XX% sul prodotto meno caro da un paniere 222",
              value: "XX% sul prodotto meno caro da un paniere 222",
            },
          ],
          allowMultiple: true,
        },
        shortcut: true,
      },
      {
        key: "priorita",
        label: "Priorita",
        filterConfig: {
          type: "choice",
          mappedColumn: 2,
          items: [
            { label: "1", value: 1 },
            { label: "2", value: 2 },
            { label: "3", value: 3 },
            { label: "4", value: 4 },
            { label: "5", value: 5 },
          ],
          allowMultiple: true,
        },
        shortcut: true,
      },
      {
        key: "stato",
        label: "Stato",
        filterConfig: {
          type: "choice",
          mappedColumn: 3,
          items: [
            { label: "Attiva", value: "Attiva" },
            { label: "In lavorazione", value: "In lavorazione" },
            { label: "Disattivata", value: "Disattivata" },
          ],
          allowMultiple: true,
        },
        shortcut: true,
      },
    ],
  };

  return (
    <Page fullWidth>
      <Card>
        <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}></Tabs>
        <DataTable
          columns={dataTableConfig.columns}
          rows={rows}
          filters={dataTableConfig.filters}
        />
      </Card>
    </Page>
  );
}
